import os
import discord
import logging

from discord.ext import commands
from dotenv import load_dotenv
from typing import Final
from game import Game

from gameworld import construct_universe

CHANNEL_NAME : Final = "gravy-rpg"


load_dotenv()

bot = commands.Bot(command_prefix='>')

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))

logger.addHandler(handler)


universe = construct_universe()
universe.is_valid()


class KoreClient(discord.Client):
    async def on_ready(self):
        for guild in self.guilds:
            if guild.id == GUILD:
                break

        self.game = Game()

    async def on_member_join(self, member):
        await member.create_dm()
        await member.dm_channel.send(
            f'Hi {member.name}, welcome to this discord server, feel free to try out the RPG game running in the `gravy-rpg` channel'
        )

    async def on_message(self, message):
        if message.author == self.user:
            return

        if message.channel != CHANNEL_NAME:
            return

        print(message.content)
        print(message.channel)
        print(message.author)

    # await self.game.interpret_command(
    #     message.content,
    #     message.channel,
    #     message.author
    # )

    async def clear(self, ctx, number):
        msgs = []
        number = int(number)
        async for x in self.logs_from(ctx.message.channel, limit=number):
            msgs.append(x)

        await self.delete_messages(msgs)


if __name__ == "__main__":
    pass
    # TOKEN = os.getenv('DISCORD_BOT_TOKEN')
    # GUILD = os.getenv('DISCORD_SERVER_NAME')

    # client = KoreClient()
    # client.run(TOKEN)