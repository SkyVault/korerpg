import sqlite3
import pickle
import os


class Game:
    def __init__(self):
        self.players = []

        self.load_players_from_db()

    def load_players_from_db(self):
        if not os.path.exists("data"):
            # First load the data
            pass
        else:
            data = pickle.load("data")