# Okay so how is this all going to be laid out...

## Requirements

Player needs to be attached to a location

locations need to be a recursive structure

         +- Parent loc    +----- Each parent room will be the main foyer with off shoot rooms
	 |                |
         V                V
Galaxy > Planet > [ Dock, Pub > [ Office, Bathroom, ], Spaceship shop ]
                    ^           ^
                    |           |
                    |           +---- Children loc list
		    +-- Each planet / station will have a dock, every other location when the back command is issued, will link back to the dock

Each planet will have a dock, you can only take off and land on docks

At each room, you can say "back" and that will take you out to the parent

Each position is relative to its parent