# Items

There will be a lot of unique and cool items to trade and find

## Consumables

### Illegal

Of course there will be drugs

1. Scoomy - A type of fungus that mellows out emotions while extending the life of the user
	* 2 strains, Defa, Indefa... one is a headhigh and the other is a body
2. Skiev - Dried Scoomy crystals

3. Ketamyth - A super drug allowing for super strength and intense energy

### Legal

1. Tang Brew - A light beer from the Tang
2. Moonshine - A hard liquor
3. Bloodshot - A hard liquor
