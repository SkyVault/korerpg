from typing import Final
from enum import Enum, auto


class Location:
    def __init__(self, name="Location", children=[], parent=None):
        self.name = name
        self.children = children
        self.parent = None
        self.position = (0, 0)

    def add_location(self, loc):
        self.children.append(loc)
        loc.parent = self

    def is_valid(self):
        return True


class Universe(Location):
    def __init__(self, locations=[]):
        super().__init__("Universe", locations)
        self.position = (0, 0)

    def is_valid(self):
        for galaxy in self.children:
            if not galaxy.is_valid():
                return False
            return True


class Galaxy(Location):
    def is_valid(self):
        for location in self.children:
            if not location.is_valid():
                return
        return True


class Planet(Location):
    def is_valid(self):
        for loc in self.children:
            if type(loc) is Dock:
                return True
        print(f"Planet '{self.name}' in the galaxy '{self.parent.name}' is in-valid, missing dock.")
        return False


class SpaceStation(Planet):
    pass


class Dock(Location):
    pass


class Building(Location):
    pass


def get_galaxies():
    yaniv = Galaxy(name="Yaniv")
    yaniv.position = (3000, 3000)

    goyouxPlanet = Planet(name="Goyoux")
    goyouxPlanet.position = (-100, -100)

    goyouxDock = Dock(name="Dock of Goyoux")

    goyouxPlanet.add_location(goyouxDock)
    yaniv.add_location(goyouxPlanet)

    return [yaniv]


def construct_universe():
    result = Universe()

    for galaxy in get_galaxies():
        result.add_location(galaxy)

    return result
